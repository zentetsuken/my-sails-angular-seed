#====================================
#=== App structure ==================
#====================================

App
  |- backend/     <=== Sails app (Use Sails generator)
  |- frontend/    <=== Angular app (Use Yeoman Angular generator)

#====================================
#=== Backend ========================
#=== SailsJS ========================
#====================================

Database
  - MongoDB with basic authentication

Model
  - User
    = Fields
      -- username: string
      -- password: string
      -- email: email
      -- displayName: string
      -- roles: array(Role)
        == default: []
    = Validations
      -- unique
        == username
        == email
      -- required
        == username
        == password
        == displayName
        == roles
      -- email
        == email
    = Callbacks
      -- BeforeCreate 
        == use scrypt to encode password
    = Override
      -- toJSON: to remove password field when return JSON
  - Role
    = Fields
      -- name

Controller
  - UserController
    = RESTful
    = Register
  - AuthController
    = Authenticate
      -- Use json web token (jwt) authentication method

Policy
  - isAuthenticate
    = User
      -- Update
      -- Delete

Service
  - tokenAuth
    = issueToken
    = verifyToken


#====================================
#=== Frontend =======================
#=== AngularJS ======================
#====================================

Configuration

Routing
  - ui-router (https://github.com/angular-ui/ui-router)
  
Constant
  - AUTH_EVENTS
  - USER_ROLES

Service
  - AuthService
  - Session
  - AuthInterceptor

Controller
  - Application
  - Home
  - Login

Directive
  - Login modal

View
  - Home
  - Login
  
#====================================
#=============Credit=================
#====================================

Many thanks to Alberto Pose for his blog about Cookies vs Token, and a great sample codes at https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/

Thanks to Gert Hengeveld for his blog post about authentication technique at https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

And the great example about JSON Web Token authentication from angular-tip blog at http://angular-tips.com/blog/2014/05/json-web-tokens-examples/
Most of the codes are from this blog, many thanks to them.
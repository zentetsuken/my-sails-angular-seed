'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:ApplicationCtrl
 * @description
 * # ApplicationCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
  .controller('ApplicationCtrl', function ($scope, $rootScope, $state, AUTH_EVENTS, USER_ROLES, AuthService) {
    $scope.currentUser = null;
    $scope.userRoles = USER_ROLES;
    $scope.isAuthorized = AuthService.isAuthorized;
    $scope.isAuthenticated = AuthService.isAuthenticated;
   
    $scope.setCurrentUser = function (user) {
      $scope.currentUser = user;
    };

    $scope.logout = function () {
      if (AuthService.logout()) {
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
      } else {
        // TODO: Add some notification method, eg. sticky on top right corner
        // or modal error, blah~~.
      }
    };


    // AUTH_EVENTS management =================

    $scope.$on(AUTH_EVENTS.loginSuccess, function () {
      $state.go('dashboard');
    });

    $scope.$on(AUTH_EVENTS.loginFailed, function () {
      $state.go('login');
    });

    $scope.$on(AUTH_EVENTS.logoutSuccess, function () {
      $state.go('login');
    });

    $scope.$on(AUTH_EVENTS.sessionTimeout, function () {
      $state.go('login');
    });

    $scope.$on(AUTH_EVENTS.notAuthenticated, function () {
      $state.go('login');
    });

    $scope.$on(AUTH_EVENTS.notAuthorized, function () {
      $state.go('login');
    });
  });

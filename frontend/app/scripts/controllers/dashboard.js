'use strict';

angular.module('frontendApp')
  .controller('DashboardCtrl', function ($scope) {

    $scope.charts = {
      types: {
        area: 'area',
        bar: 'bar',
        pie: 'pie'
      },
      area: {
        config: {
          labels: false,
          title: 'Products',
          legend: {
            display: true,
            position: 'right'
          },
          innerRadius: 0,
          lineLegend: 'lineEnd'
        },
        style: {
          width: '100%',
          height: '200px'
        }
      },
      bar: {
        config: {
          labels: false,
          title: 'Products',
          legend: {
            display: true,
            position: 'right'
          },
          innerRadius: 0,
          lineLegend: 'lineEnd'
        },
        style: {
          width: '100%',
          height: '300px'
        }
      },
      pie: {
        config: {
          labels: false,
          title: 'Products',
          legend: {
            display: false,
            position: 'right'
          },
          innerRadius: '50%',
          lineLegend: 'lineEnd'
        },
        style: {
          width: '100%',
          height: '300px'
        }
      },
      data: {
        series: ['Sales', 'Income', 'Expense'],
        data: [{
          x: 'Laptops',
          y: [100, 500, 0],
          tooltip: 'this is tooltip'
        }, {
          x: 'Desktops',
          y: [300, 100, 100]
        }, {
          x: 'Mobiles',
          y: [351]
        }, {
          x: 'Tablets',
          y: [54, 0, 879]
        }]
      }
    };
  });

'use strict';

angular.module('frontendApp')
  .controller('RegisterCtrl', function ($scope, $rootScope, AUTH_EVENTS, AuthService) {
    $scope.newUser = {
      username: '',
      password: '',
      confirmPassword: ''
    };
    $scope.register = function (newUser) {
      AuthService.register(newUser).then(function (user) {
        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        $scope.setCurrentUser(user);
      }, function () {
        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
      });
    };
  });

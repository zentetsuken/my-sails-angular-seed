'use strict';

angular.module('frontendApp')
  .directive('metisMenu', function () {
    return {
      restrict: 'A',
      replace: false,
      link: function (scope, elem, attrs) {
        angular.element(elem).metisMenu(scope.$eval(attrs.metisMenu));
      }
    };
  });
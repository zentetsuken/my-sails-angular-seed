'use strict';

angular.module('frontendApp')
  .constant('USER_ROLES', {
    admin: 'admin',
    user: 'user'
  });
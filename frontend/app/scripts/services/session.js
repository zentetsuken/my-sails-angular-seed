'use strict';

/**
 * @ngdoc service
 * @name frontendApp.Session
 * @description
 * # Session
 * Service in the frontendApp.
 */
angular.module('frontendApp')
  .service('Session', function Session() {
    this.token = null;
    this.user = null;

    this.create = function (user, token) {
      this.token = token;
      this.user = user;
    };
    this.destroy = function () {
      this.token = null;
      this.user = null;

      return (this.token === null && this.user === null);
    };
    this.isAuthorized = function (roles) {
      // return true;
      if (this.user === null || this.token === null) {
        return false;
      }
      if (!angular.isArray(this.user.roles)) {
        return false;
      }
      return this.user.roles.some(function (elem) {
        return roles.indexOf(elem) !== -1;
      });
    };
    return this;
  });

'use strict';

/**
 * @ngdoc service
 * @name frontendApp.AuthService
 * @description
 * # AuthService
 * Factory in the frontendApp.
 */
angular.module('frontendApp')
  .factory('AuthService', function ($http, Session) {
    var authService = {};
 
    authService.login = function (credentials) {
      return $http
        .post('/auth/authenticate', credentials)
        .then(function success (res) {
          Session.create(res.data.user, res.data.token);
          return res.data.user;
        });
    };

    authService.register = function (user) {
      return $http
        .post('/user/register', user)
        .then(function (res) {
          Session.create(res.data.user, res.data.token);
          return res.data.user;
        });
    };

    authService.logout = function () {
      return Session.destroy();
    };
   
    authService.isAuthenticated = function () {
      return !!Session.token;
      // return true;
    };
   
    authService.isAuthorized = function (authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      return (authService.isAuthenticated() && Session.isAuthorized(authorizedRoles));
    };
   
    return authService;
  });

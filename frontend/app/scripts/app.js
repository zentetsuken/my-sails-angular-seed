'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
  .module('frontendApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    //'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'angularCharts'
  ])
  .config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {

    $urlRouterProvider.otherwise(function ($injector){
      $injector.get('$state').go('dashboard');
    });

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html'
      })
      .state('register', {
        url: '/register',
        templateUrl: 'views/register.html'
      })
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'views/dashboard.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
        },
        controller: 'DashboardCtrl'
      })
      .state('about', {
        url: '/about',
        templateUrl: 'views/about.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
        },
        controller: 'AboutCtrl'
      })
      .state('contact', {
        url: '/contact',
        templateUrl: 'views/contact.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
        },
        controller: 'ContactCtrl'
      })
      .state('profile', {
        url: '/profile',
        templateUrl: 'views/profile.html',
        data: {
          authorizedRoles: [USER_ROLES.admin, USER_ROLES.user]
        },
        controller: 'ProfileCtrl'
      });
  })
  .config(function ($httpProvider) {
    $httpProvider.interceptors.push(['$injector', function ($injector) {
        return $injector.get('AuthInterceptor');
      }
    ]);
  })
  .run(function ($rootScope, $state, AUTH_EVENTS, AuthService){

    $rootScope.$on('$stateChangeStart', function (event, next) {

      if(next.data !== undefined) {
        var authorizedRoles = next.data.authorizedRoles;
        if (!AuthService.isAuthorized(authorizedRoles)) {
          event.preventDefault();
          if (AuthService.isAuthenticated()) {
            // user is not allowed
            $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
          } else {
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
          }
        }
      }

    });

  });

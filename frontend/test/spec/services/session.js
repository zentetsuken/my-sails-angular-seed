'use strict';

describe('Service: Session', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var Session;
  var USER_ROLES;
  beforeEach(inject(function (_USER_ROLES_, _Session_) {
    Session = _Session_;
    USER_ROLES = _USER_ROLES_;

    Session.create({username: 'me', password: 'a', roles: [USER_ROLES.user]}, 'Some token string');
  }));

  afterEach(function () {
    Session.destroy();
  });

  it('should create user credentials', function () {
    expect(Session.user).not.toBe(null);
    expect(Session.token).not.toBe(null);
  });

  it('should destroy user credentials', function () {
    var result = Session.destroy();
    expect(Session.user).toBe(null);
    expect(Session.token).toBe(null);
    expect(result).toBe(true);
  });

  describe('authorize', function () {
    it('should authorize user role', function () {
      expect(Session.isAuthorized([USER_ROLES.user])).toBe(true);
    });

    it('should not authorize admin role', function () {
      expect(Session.isAuthorized([USER_ROLES.admin])).toBe(false);
    });
  });

});

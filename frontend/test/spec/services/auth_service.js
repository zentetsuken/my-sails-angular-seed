'use strict';

describe('Service: AuthService', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var AuthService;
  var USER_ROLES;
  var $httpBackend;
  beforeEach(inject(function (_$httpBackend_, _USER_ROLES_, _AuthService_) {
    $httpBackend = _$httpBackend_;
    AuthService = _AuthService_;
    USER_ROLES = _USER_ROLES_;

    $httpBackend.resetExpectations();
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('login', function () {

    var baseUrl = '/auth/authenticate';

    it('should success', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond({user: {username: 'me'}, token: 'Some Random String'});

      var promise = AuthService.login({username: 'me', password: 'haha'});

      $httpBackend.flush();

      promise.then(function (user) {
        expect(user.username).toBe('me');
      });
    });

    it('should not authorized', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond(401, {err: 'Not authorized'});

      var promise = AuthService.login({username: 'me', password: 'wrongpassword'});

      $httpBackend.flush();

      promise.catch(function (err) {
        expect(err.status).toEqual(401);
      });
    });

    it('should forbidden', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond(403, {err: 'Forbidden'});

      var promise = AuthService.login({username: 'me', password: 'wrongpassword'});

      $httpBackend.flush();

      promise.catch(function (err) {
        expect(err.status).toEqual(403);
      });
    });

  });

  describe('register', function () {

    var baseUrl = '/user/register';

    it('should success', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond({user: {username: 'me'}, token: 'Some Random String'});

      var promise = AuthService.register({username: 'me', password: 'haha', confirmPassword: 'haha'});

      $httpBackend.flush();

      promise.then(function (user) {
        expect(user.username).toBe('me');
      });
    });

    it('should not authorized', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond(401, {err: 'Not authorized'});

      var promise = AuthService.register({username: 'me', password: 'a', confirmPassword: 'not a'});

      $httpBackend.flush();

      promise.catch(function (err) {
        expect(err.status).toEqual(401);
      });
    });

    it('should cause internal server error', function () {
      $httpBackend.expectPOST(baseUrl)
        .respond(500, {err: 'Internal server error'});

      var promise = AuthService.register({username: 'Not unique username', password: 'a', confirmPassword: 'a'});

      $httpBackend.flush();

      promise.catch(function (err) {
        expect(err.status).toEqual(500);
      });
    });

  });

  describe('logout', function () {
    it('should logout success', function () {
      expect(AuthService.logout()).toBe(true);
    });
  });

  describe('is authenticated', function () {
    it('should be authenticated', function () {
      $httpBackend.expectPOST('/auth/authenticate')
        .respond({user: {username: 'me', displayName: 'you'},token: 'Some token'});

      AuthService.login({username: 'me', password: 'mepassword'});

      $httpBackend.flush();

      expect(AuthService.isAuthenticated()).toBe(true);
    });

    it('should not be authenticated', function () {
      expect(AuthService.isAuthenticated()).toBe(false);
    });
  });

  describe('is authorized', function () {

    beforeEach(function login() {
      $httpBackend.expectPOST('/auth/authenticate')
        .respond({user: {username: 'me', displayName: 'you', roles: [USER_ROLES.user]}, token: 'Some token'});

      AuthService.login({username: 'me', password: 'mepassword'});

      $httpBackend.flush();
    });

    it('should be authorized', function () {
      expect(AuthService.isAuthorized(USER_ROLES.user)).toBe(true);
    });

    it('should not be authorized', function () {
      expect(AuthService.isAuthorized(USER_ROLES.admin)).toBe(false);
    });
  });

});

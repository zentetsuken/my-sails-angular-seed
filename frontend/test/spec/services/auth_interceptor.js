'use strict';

describe('Service: AuthInterceptor', function () {

  // load the service's module
  beforeEach(module('frontendApp'));

  // instantiate service
  var $rootScope;
  var $scope;
  var $http;
  var $httpBackend;
  var AUTH_EVENTS;
  var Session;
  var token = 'Some token string';
  var url = '/path/to/somewhere';
  beforeEach(inject(function (_$rootScope_, _$http_, _$httpBackend_, _AUTH_EVENTS_, _Session_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $http = _$http_;
    $httpBackend = _$httpBackend_;
    AUTH_EVENTS = _AUTH_EVENTS_;
    Session = _Session_;

    Session.create({username: 'me', password: 'a'}, token);

    $httpBackend.resetExpectations();
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  describe('request', function () {
    it('should have authentication token in header', function () {

      var headerExpectation = {
        'Accept': 'application/json, text/plain, */*',
        'Authorization': 'Bearer ' + token
      };

      $httpBackend.expect('POST', url, null, headerExpectation)
        .respond(200);

      $http.post(url);
      $httpBackend.flush();
    });

    it('should not have authentication token in header', function () {
      var headerExpectation = {
        'Accept': 'application/json, text/plain, */*'
      };

      $httpBackend.expectPOST(url, null, headerExpectation)
        .respond(200);

      expect(Session.destroy()).toBe(true);

      $http.post(url);
      $httpBackend.flush();
    });
  });

  describe('respond', function () {
    describe('error', function () {
      it('should broadcast "not authenticated" event from $rootScope', function () {
        $httpBackend.expectPOST(url)
          .respond(401, {some: 'thing'});

        $scope.$on(AUTH_EVENTS.notAuthenticated, function (event) {
          expect(event.targetScope).toBe($rootScope);
        });

        $http.post(url, {some: 'thing'});
        $httpBackend.flush();
      });
    });
  });

});

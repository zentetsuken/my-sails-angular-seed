/**
 * User
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

var scrypt = require('scrypt');
var scryptParameters = scrypt.params(0.1);

scrypt.hash.config.keyEncoding = "ascii";
// scrypt.hash.config.outputEncoding = "ascii";

scrypt.verify.config.keyEncoding = "ascii";
// scrypt.verify.config.hashEncoding = "ascii";

const USER_ROLES = {
  admin: 'admin',
  user: 'user'
};

module.exports = {

  attributes: {
  	
  	username: {
      type: 'string',
      unique: true,
      required: true
    },

    password: {
      type: 'string',
      required: true
    },

    email: {
      type: 'string',
      unique: true,
      email: true
    },

    displayName: {
      type: 'string',
      required: true
    },

    roles: {
      type: 'array',
      required: true,
      defaultsTo: [USER_ROLES.user]
    },

    // Override toJSON instance method
    // to remove password value
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },

  roles: USER_ROLES,

  beforeValidation: function (values, next) {
    if (values.displayName === undefined) {
      values.displayName = values.username;
    }
    return next();
  },

  beforeCreate: function (values, next) {
    scrypt.hash(values.password, scryptParameters, function(err, result){
      if(err) return next(err);
      values.password = result;
      return next();
    });
  },

  validPassword: function (password, user, cb) {
    scrypt.verify(user.password.buffer, password, function(err, result) {
      if (err) return cb(err);
      return cb(null, result);
    });
  }

};

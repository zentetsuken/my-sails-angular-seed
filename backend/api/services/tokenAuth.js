var jwt = require('jsonwebtoken');

var SECRET = process.env.TOKEN_SECRET || "Th15 |s My 53cr3t";

module.exports = {
  issueToken: function (payload) {
    return jwt.sign(payload, SECRET);
  },

  verifyToken: function (token, verified) {
    return jwt.verify(token, SECRET, {}, verified);
  }
};
/**
 * UserController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  register: function (req, res) {
    var username = req.param('username');
    var password = req.param('password');
    var confirmPassword = req.param('confirmPassword');

    if (password === '') {
      return res.json(401, {err: 'Password requried'});
    }

    if (password !== confirmPassword) {
      return res.json(401, {err: 'Password doesn\'t match'});
    }

    User.create({username: username, password: password}).exec(function (err, user) {
      if (err) {
        return res.json(500, {err: err});
      }
      if (user) {
        return res.json({user: user, token: tokenAuth.issueToken(user.id)});
      }
    });
  },


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to UserController)
   */
  _config: {}

  
};
